# import networkx as nx
# import matplotlib.pyplot as plt
#
# T = nx.ranodm_tree(20)
# T.add_edge(40,20)
# T.add_node(100)
#
# for n in T.neighbors(0):
#  print(n)
#
# nx.draw_networkx(T)
# plt.savefig("graf.png")
# plt.show()

class Node:
 def __init__(self, k):
  self.key = k
  self.next = None
  self.prev = None


class BST:
 def __init__(self):
  self.root = None
 def BSTsearchR(self,x,k):
  # szuka rekurencyjnie wezla zawierajacego klucz k
  # w poddrzewie o korzeniu x
  if x==None or x.key==k:
   return x # None oznacza, ze szukanego klucza
  # nie ma w drzewie
  if k<x.key:
   return self.BSTsearchR(x.left,k)
  else:
   return self.BSTsearchR(x.right,k)

 def BSTinsert(self, z):
   # wstawia wezel z do drzewa
   x = self.root
   y = None  # y jest ojcem x
   while x != None:
    y = x
    if z.key < x.key:
     x = x.left
    else:
     x = x.right
   z.p = y
   if y == None:  # drzewo puste
    self.root = z
   else:
    if z.key < y.key:
     y.left = z
    else:
     y.right = z

 def bstDelete(self,z):
  # usuwa węzeł"z" z drzewa
  # wersja "naturalna"
  if z.left==None and z.right==None: # (1) z liść
   if z==self.root:
    self.root=None
   else:
    if z==z.p.left: # z jest lewym synem
     z.p.left=None
    else:
     z.p.right=None
  elif z.left != None and z.right != None: # (3) dwóch synów
   y=self.bstMinimum(z.right)
   z.key=y.key # przepisz zawartość węzła y do z
   self.bstDelete(y) # przypadek (1) lub (2)
  else:  # (2) jeden syn
   if z.left != None:  # z.right==None
    z.left.p = z.p
    if z == self.root:
     self.root = z.left
    else:
     if z == z.p.left:
      z.p.left = z.left
     else:
      z.p.right = z.left
   else:  # z.left==None
    z.right.p = z.p
    if z == self.root:
     self.root = z.right
    else:
     if z == z.p.left:
      z.p.left = z.left
     else:
      z.p.right = z.left

 def BSTinOrderD(self,x,d):
  # “d” to głębokość na której jest węzeł “x”
  if x==None: return
  self.BSTinOrderD(x.left, d+1)
  print(x.key)
  self.BSTinOrderD(x.right,d+1)

t= [1,5,7,4,12,8,9,3,0]
BST.BSTinsert(Node("nazwisko"))
