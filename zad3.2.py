from timeit import default_timer as timer

def quickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q)
        quickSort(A, q+1, r)
    return A
def partition(A, p, r):
    x = A[r] # element wyznaczajacy podział
    i = p-1
    for j in range(p, r+1):
        if A[j] <= x:
            i = i+1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i-1

A1 = [1, 3, 4, 2, 6, 12, 9, 4]
r1 = len(A1) - 1

# print(quickSort(A1, 0, r1))
A_rosnace = []
A_malejace = []
for i in range(1000):
    A_rosnace.append(i)
r2 = len(A_rosnace) - 1
# print(A_rosnace)
for i in range(1000,0,-1):
    A_malejace.append(i)
r3 = len(A_malejace) - 1
# print(A_malejace)

start = timer()
quickSort(A1,0,r1)
stop = timer()
Tn = stop - start
print(r1+1, Tn)